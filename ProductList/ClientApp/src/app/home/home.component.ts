import { Component, OnInit } from '@angular/core';
import { CategoryDataServices } from '../API-services/category.data.service';
import { Category } from '../models';
import { Field } from '../models';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [CategoryDataServices]
})
export class HomeComponent implements OnInit {

  categories: Category[];
  fields: Array<Field> = [];

  constructor(
    private categoryDataServices: CategoryDataServices
  ) { }

  ngOnInit(): void {
    this.categoryDataServices.getCategories().subscribe(
      (data: Category[]) => this.categories = data
    );
  }

  addCategoryField(): void {
    let newField = new Field();
    newField.id = null;
    newField.name = "test";
    newField.fieldValues = [];
    this.fields.push(newField);
  }

  openModal(): void {
    this.fields = [];
  }

  saveCategory(): void {}
}

