export class Category {
  constructor(
  public id?: number,
  public name?: string,
  public fields?: Array<Field>
  ) { }
}
export class Field {
  constructor(
    public id?: number,
    public name?: string,
    public fieldValues?: Array<FieldValue>
  ) { }
}
export class CategoryField {
  constructor(
    public id?: number,
    public cutegory?: Category,
    public field?: Field
  ) { }
}
export class FieldValue {
  constructor(
    public id?: number,
    public field?: Field,
    public value?: string
  ) { }
}
export class Product {
  constructor(
    public id?: number,
    public name?: string,
    public price?: number,
    public desciption?: string,
    public cutegory?: Category,
    public fieldValues?: Array<FieldValue>
  ) { }
}
export class ProductFieldValue {
  constructor(
    public id?: number,
    public product?: Product,
    public fieldValue?: FieldValue
  ) { }
}
