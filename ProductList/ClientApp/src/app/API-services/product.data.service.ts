import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

import { Product } from '../models';

@Injectable({
  providedIn: 'root'
})

export class ProductDataServices {

  private url = 'https://localhost:44381/api/Products';

  constructor(private http: HttpClient) {
  }

  getProducts() {
    return this.http.get(this.url);
  }

  getProduct(id: number) {
    return this.http.get(this.url + '/' + id);
  }

  createPatient(p: Product) {
    return this.http.post(this.url, p);
  }

  deletePatient(id: number) {
    return this.http.delete(this.url + '/' + id);
  }
}
