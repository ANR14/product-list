import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Category } from '../models';


@Injectable({
  providedIn: 'root'
})

export class CategoryDataServices {

  //private url: string = "/assets/db.json";
  private url = 'https://localhost:44381/api/Categories';

  constructor(private http: HttpClient) {
  }

  getCategories(): Observable<Category[]> {
    return this.http.get(this.url).pipe(
      map(data => {
        let categoryList = data['categoryList'];
        return categoryList.map(function(category: any) {
          return {
            id: category.id,
            name: category.name,
            fields: category.fields
          };
        });
      })
    );
  }
}
