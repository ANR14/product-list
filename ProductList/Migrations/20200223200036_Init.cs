﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ProductList.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Fields",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fields", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<float>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CategoryFields",
                columns: table => new
                {
                    CategoryId = table.Column<int>(nullable: false),
                    FieldId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryFields", x => new { x.CategoryId, x.FieldId });
                    table.ForeignKey(
                        name: "FK_CategoryFields_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryFields_Fields_FieldId",
                        column: x => x.FieldId,
                        principalTable: "Fields",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FieldValues",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Value = table.Column<string>(nullable: true),
                    FieldId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FieldValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FieldValues_Fields_FieldId",
                        column: x => x.FieldId,
                        principalTable: "Fields",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductFieldValues",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false),
                    FieldValueId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductFieldValues", x => new { x.ProductId, x.FieldValueId });
                    table.ForeignKey(
                        name: "FK_ProductFieldValues_FieldValues_FieldValueId",
                        column: x => x.FieldValueId,
                        principalTable: "FieldValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductFieldValues_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Сок" },
                    { 2, "Шоколад" }
                });

            migrationBuilder.InsertData(
                table: "Fields",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Брэнд" },
                    { 2, "Вкус" },
                    { 3, "Объем" },
                    { 4, "Сорт" },
                    { 5, "Вес" }
                });

            migrationBuilder.InsertData(
                table: "CategoryFields",
                columns: new[] { "CategoryId", "FieldId" },
                values: new object[,]
                {
                    { 2, 5 },
                    { 1, 1 },
                    { 2, 1 },
                    { 2, 4 },
                    { 1, 2 },
                    { 1, 3 }
                });

            migrationBuilder.InsertData(
                table: "FieldValues",
                columns: new[] { "Id", "FieldId", "Value" },
                values: new object[,]
                {
                    { 4, 2, "Яблоко" },
                    { 9, 4, "Молочный" },
                    { 8, 4, "Темный" },
                    { 7, 3, "2" },
                    { 6, 3, "1" },
                    { 11, 5, "200" },
                    { 10, 5, "220" },
                    { 3, 1, "Аленка" },
                    { 2, 1, "Казахстан" },
                    { 1, 1, "Да-Да" },
                    { 5, 2, "Ананас" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Description", "Name", "Price" },
                values: new object[,]
                {
                    { 4, 2, "Шоколад Аленка, Молочный, 200 гр.", "Шоколад Аленка, Молочный, 200 гр.", 700f },
                    { 3, 2, "Шоколад Казахстан, Темный, 220 гр.", "Шоколад Казахстан, Темный, 220 гр.", 500f },
                    { 2, 1, "Сок Да-Да, Ананас, 2Л", "Сок Да-Да, Ананас, 2Л", 800f },
                    { 1, 1, "Сок Да-Да, Яблоко, 1Л", "Сок Да-Да, Яблоко, 1Л", 400f }
                });

            migrationBuilder.InsertData(
                table: "ProductFieldValues",
                columns: new[] { "ProductId", "FieldValueId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 2 },
                    { 4, 3 },
                    { 1, 4 },
                    { 2, 5 },
                    { 1, 6 },
                    { 2, 7 },
                    { 3, 8 },
                    { 4, 9 },
                    { 3, 10 },
                    { 4, 11 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CategoryFields_FieldId",
                table: "CategoryFields",
                column: "FieldId");

            migrationBuilder.CreateIndex(
                name: "IX_FieldValues_FieldId",
                table: "FieldValues",
                column: "FieldId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductFieldValues_FieldValueId",
                table: "ProductFieldValues",
                column: "FieldValueId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CategoryFields");

            migrationBuilder.DropTable(
                name: "ProductFieldValues");

            migrationBuilder.DropTable(
                name: "FieldValues");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Fields");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
