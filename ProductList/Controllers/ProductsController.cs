﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProductList.Models;

namespace ProductList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProductListContext _context;

        public ProductsController(ProductListContext context)
        {
            _context = context;
        }

        // GET: api/Products/ 
        [HttpGet]
        public IEnumerable<Product> GetProduct([FromQuery] Category category)
        {
            using (ProductListContext db = new ProductListContext())
            {
                var result = new List<Product>();
                if (category.CategoryFields.Count > 0)
                {
                    var categoryFields = category.CategoryFields;
                    foreach (CategoryField categoryField in categoryFields)
                    {
                        var fieldValues = categoryField.Field.FieldValues.ToList();
                        foreach (FieldValue fieldValue in fieldValues)
                        {
                            var product = db.Products.Where(p => p.CategoryId == category.Id).
                                                Include(p => p.ProductFieldValues).
                                                    ThenInclude(p => p.FieldValue).
                                                        ThenInclude(fv => fv.Id == fieldValue.Id).
                                                        ToList();
                            result.AddRange(product);
                        }
                    }
                    return result;
                }
                else
                {
                    var product = db.Products.Where(p => p.CategoryId == category.Id).
                                        Include(p => p.ProductFieldValues).
                                            ThenInclude(p => p.FieldValue).
                                                ThenInclude(fv => fv.Field);
                    result.AddRange(product);
                    return result;
                }
            }            
        }

        // POST: api/Products
        [HttpPost]
        public async Task<IActionResult> PostProduct([FromBody] List<ProductFieldValue> productFieldValues)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Categories.Add(productFieldValues.First().Product.Category);
            _context.Products.Add(productFieldValues.First().Product);
            foreach (ProductFieldValue productFieldValue in productFieldValues)
            {
                if (_context.FieldValues.Any(f => f.Id == productFieldValue.FieldValue.Id) != false)
                {
                    _context.FieldValues.Add(productFieldValue.FieldValue);
                }
                _context.ProductFieldValues.Add(productFieldValue);
            }

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProduct", new { id = productFieldValues.First().Product.Id }, 
                productFieldValues.First().Product);
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return Ok(product);
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}