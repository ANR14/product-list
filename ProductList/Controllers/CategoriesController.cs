﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProductList.Models;

namespace ProductList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ProductListContext _context;

        public CategoriesController(ProductListContext context)
        {
            _context = context;
        }

        // GET: api/Categories
        [HttpGet]
        public async Task<IActionResult> GetCategories()
        {
            using (ProductListContext db = new ProductListContext())
            {
                var categories = await db.Categories.Select(c => new
                    {
                        c.Id,
                        c.Name,
                        Fields = c.CategoryFields.Select(cf => cf.Field).ToList()
                    }).ToListAsync();
                return Ok(new { categoryList = categories });
            }
        }

        // POST: api/Categories
        [HttpPost]
        public async Task<IActionResult> PostCategory([FromBody] Category Category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (ProductListContext db = new ProductListContext())
            {
                db.Categories.Add(Category);
                await db.SaveChangesAsync();
            }

            return CreatedAtAction("GetCategory", new { id = Category.Id }, Category);
        }

        //// DELETE: api/Categories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (ProductListContext db = new ProductListContext())
            {
                var Category = db.Categories.
                    Include(c => c.CategoryFields).
                    Include(c => c.Products).
                        ThenInclude(p => p.ProductFieldValues).
                    FirstOrDefault(c => c.Id == id);
                db.Categories.Remove(Category);
                await db.SaveChangesAsync();
                return Ok(Category);
            }
        }
    }
}