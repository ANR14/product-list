﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductList.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<CategoryField> CategoryFields { get; set; }
        public List<Product> Products { get; set; }

        public Category()
        {
            CategoryFields = new List<CategoryField>();
            Products = new List<Product>();
        }
    }
}
