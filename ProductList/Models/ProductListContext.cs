﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductList.Models
{
    public class ProductListContext : DbContext
    {
        public ProductListContext()
        {
        }

        public ProductListContext(DbContextOptions<ProductListContext> options)
            : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Field> Fields { get; set; }
        public DbSet<FieldValue> FieldValues { get; set; }
        public DbSet<CategoryField> CategoryFields { get; set; }
        public DbSet<ProductFieldValue> ProductFieldValues { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=prodlistdb;Username=postgres;Password=q1W@e3R$");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CategoryField>()
               .HasKey(cf => new { cf.CategoryId, cf.FieldId });

            modelBuilder.Entity<CategoryField>()
                .HasOne(cf => cf.Category)
                .WithMany(c => c.CategoryFields)
                .HasForeignKey(cf => cf.CategoryId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CategoryField>()
                .HasOne(cf => cf.Field)
                .WithMany(f => f.CategoryFields)
                .HasForeignKey(cf => cf.FieldId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ProductFieldValue>().
                HasKey(pf => new { pf.ProductId, pf.FieldValueId });

            modelBuilder.Entity<ProductFieldValue>()
                .HasOne(pf => pf.Product)
                .WithMany(p => p.ProductFieldValues)
                .HasForeignKey(pf => pf.ProductId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ProductFieldValue>()
                .HasOne(pf => pf.FieldValue)
                .WithMany(f => f.ProductFieldValues)
                .HasForeignKey(pf => pf.FieldValueId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Product>()
                .HasOne(p => p.Category)
                .WithMany(c => c.Products)
                .HasForeignKey(p => p.CategoryId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<FieldValue>()
               .HasOne(fv => fv.Field)
               .WithMany(f => f.FieldValues)
               .HasForeignKey(fv => fv.FieldId)
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Category>().HasData(
                new Category[]
                {
                new Category { Id=1, Name="Сок"},
                new Category { Id=2, Name="Шоколад"}
                });
            modelBuilder.Entity<Field>().HasData(
                new Field[]
                {
                new Field { Id=1, Name="Брэнд"},
                new Field { Id=2, Name="Вкус"},
                new Field { Id=3, Name="Объем"},
                new Field { Id=4, Name="Сорт"},
                new Field { Id=5, Name="Вес"}
                });
            modelBuilder.Entity<CategoryField>().HasData(
                new CategoryField[]
                {
                new CategoryField { CategoryId=1, FieldId=1},
                new CategoryField { CategoryId=1, FieldId=2},
                new CategoryField { CategoryId=1, FieldId=3},
                new CategoryField { CategoryId=2, FieldId=1},
                new CategoryField { CategoryId=2, FieldId=4},
                new CategoryField { CategoryId=2, FieldId=5}
                });
            modelBuilder.Entity<FieldValue>().HasData(
                new FieldValue[]
                {
                new FieldValue { Id=1, Value="Да-Да", FieldId=1},
                new FieldValue { Id=2, Value="Казахстан", FieldId=1},
                new FieldValue { Id=3, Value="Аленка", FieldId=1},
                new FieldValue { Id=4, Value="Яблоко", FieldId=2},
                new FieldValue { Id=5, Value="Ананас", FieldId=2},
                new FieldValue { Id=6, Value="1", FieldId=3},
                new FieldValue { Id=7, Value="2", FieldId=3},
                new FieldValue { Id=8, Value="Темный", FieldId=4},
                new FieldValue { Id=9, Value="Молочный", FieldId=4},
                new FieldValue { Id=10, Value="220", FieldId=5},
                new FieldValue { Id=11, Value="200", FieldId=5}
                });
            modelBuilder.Entity<Product>().HasData(
                new Product[]
                {
                new Product { Id=1, Name="Сок Да-Да, Яблоко, 1Л", Price=400, Description="Сок Да-Да, Яблоко, 1Л", CategoryId=1},
                new Product { Id=2, Name="Сок Да-Да, Ананас, 2Л", Price = 800, Description="Сок Да-Да, Ананас, 2Л", CategoryId=1},
                new Product { Id=3, Name="Шоколад Казахстан, Темный, 220 гр.", Price=500, Description="Шоколад Казахстан, Темный, 220 гр.", CategoryId=2},
                new Product { Id=4, Name="Шоколад Аленка, Молочный, 200 гр.", Price=700, Description="Шоколад Аленка, Молочный, 200 гр.", CategoryId=2}
                });
            modelBuilder.Entity<ProductFieldValue>().HasData(
                new ProductFieldValue[]
                {
                new ProductFieldValue { ProductId=1, FieldValueId=1},
                new ProductFieldValue { ProductId=2, FieldValueId=1},
                new ProductFieldValue { ProductId=3, FieldValueId=2},
                new ProductFieldValue { ProductId=4, FieldValueId=3},
                new ProductFieldValue { ProductId=1, FieldValueId=4},
                new ProductFieldValue { ProductId=2, FieldValueId=5},
                new ProductFieldValue { ProductId=1, FieldValueId=6},
                new ProductFieldValue { ProductId=2, FieldValueId=7},
                new ProductFieldValue { ProductId=3, FieldValueId=8},
                new ProductFieldValue { ProductId=4, FieldValueId=9},
                new ProductFieldValue { ProductId=3, FieldValueId=10},
                new ProductFieldValue { ProductId=4, FieldValueId=11}
                });
        }
    }
}