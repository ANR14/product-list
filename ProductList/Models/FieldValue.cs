﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductList.Models
{
    public class FieldValue
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int FieldId { get; set; }
        public Field Field { get; set; }
        public List<ProductFieldValue> ProductFieldValues { get; set; }

        public FieldValue()
        {
            ProductFieldValues = new List<ProductFieldValue>();
        }
    }
}
