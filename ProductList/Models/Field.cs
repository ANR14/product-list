﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductList.Models
{
    public class Field
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<CategoryField> CategoryFields { get; set; }
        public List<FieldValue> FieldValues { get; set; }

        public Field()
        {
            CategoryFields = new List<CategoryField>();
            FieldValues = new List<FieldValue>();
        }
    }
}
