﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductList.Models
{
    public class ProductFieldValue
    {
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int FieldValueId { get; set; }
        public FieldValue FieldValue { get; set; }
    }
}
