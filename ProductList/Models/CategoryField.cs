﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductList.Models
{
    public class CategoryField
    {
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int FieldId { get; set; }
        public Field Field { get; set; }
    }
}
